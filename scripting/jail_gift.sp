#pragma semicolon 1 

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <csgo_colors>
#include <shop>
#include <t2lrcfg>
#pragma newdecls required

bool gift[MAXPLAYERS+1];
float g_fFloodTime[MAXPLAYERS+1];				// против фарма кредитов

//int m_iHealth;

public Plugin myinfo = {
	name = "[CS:GO] Jail Gift",
	author = "Darkeneez",
	description = "Рандомные подарки",
	version = "1.2",
	url = "http://servers-info.ru"
};

public void OnPluginStart() 
{
	//if ((m_iHealth = FindSendPropOffs("CCSPlayer", "m_iHealth")) == -1) {
	//	SetFailState("m_iHealth error");
	//	return;
	//}
	RegConsoleCmd("sm_gift", Gift);
	HookEvent("player_spawn", RoundStart);
	//HookEvent("player_hurt", player_hurt);
}

public void RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	gift[client] = false;
}

public Action Gift(int client, int args)
{
	if (IsPlayerAlive(client) && !gift[client]) {
		if (!IsLrActivated())
			GiveRandomPrize(client);
		else CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Во время ЛР вы не можете получить приз");
	}
	else CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы должны быть живым или уже получили свой приз");
	
	return Plugin_Handled;
}

//public Action player_hurt(Handle event, const char[] name, bool silent)
//{
//	char item[64];
//	int client = GetClientOfUserId(GetEventInt(event, "attacker"));
//	
//	GetEventString(event, "weapon", item, 64);
//	
//	if (StrEqual("taser", item)) {
//		int victim = GetClientOfUserId(GetEventInt(event,"userid"));
//		
//		int old_health = GetEntProp(victim, Prop_Send, "m_iHealth") + GetEventInt(event, "dmg_health");
//
//		if (old_health > 99) SetEntData(victim, m_iHealth, old_health - GetRandomInt(50,99), 4, true);
//		if	(GetRandomInt(1,3) != 1) {
//			ForcePlayerSuicide(client);
//			CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Осечка :) вы тоже получили дамаг");
//			SetEntData(client, m_iHealth, GetEntProp(client, Prop_Send, "m_iHealth") - GetRandomInt(25,75), 4, true); 
//		}
//	}
//	return Plugin_Continue; 
//}


stock void GiveRandomPrize(int client, bool bNoGift=false)
{
	switch(GetRandomInt(1, bNoGift? 11:15)) {
		case 1: {
			if (GetClientTeam(client) == 3) {
				GivePlayerItem(client, "weapon_tagrenade");
				CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - ВХ граната");
			} else {
				GiveWeapon(client, "weapon_p250");
				CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - пистолет");
			}
		}
		case 2: {
			if	(GetClientTeam(client) == 3) {
				GiveRandomPrize(client);
				return;
			}
			GiveWeapon(client, "weapon_usp_silencer");
			CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - пистолет");
		}
		case 3,4,9: {
			SetEntProp(client, Prop_Send, "m_iHealth", GetClientHealth(client) + GetRandomInt(5,15));
			CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - ХП");
		}
		case 14,15,12,13: {
			if (GetGameTime() - g_fFloodTime[client] > 300.0) {		// если прошло более 5 минут с момента последнего выигрыша игрока 
				g_fFloodTime[client] = GetGameTime();
				Shop_GiveClientCredits(client, GetRandomInt(10, 80), CREDITS_BY_TRANSFER);
				CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - кредиты");
			} else GiveRandomPrize(client, true);					//  разыграть гифт без кредитов
		}
		case 6,7,11,5: {
			if	(GetClientTeam(client) == 3) {
				GivePlayerItem(client, "item_kevlar");
				SetEntData(client, FindSendPropInfo("CCSPlayer", "m_ArmorValue"), 200);
				CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - Броню");
			} else {
			GivePlayerItem(client, "item_assaultsuit");
			CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - Броню");
			}
		}
		case 8,10: {
			if	(GetClientTeam(client) == 3) {
				GiveRandomPrize(client);
				return;
			}
			GiveGrenade(client);
			CGOPrintToChat(client, "{LIGHTPURPLE}[GIFT]{LIME}Вы получили свой приз - Гранату");
		}
	}
	gift[client] = true;
}

void GiveWeapon(int client, char[] weapon)
{
	int index= GivePlayerItem(client, weapon);
	SetEntProp(index, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
	SetEntProp(index, Prop_Send, "m_iClip1", GetRandomInt(1, 2), 4, 0);
}

void GiveGrenade(int client)
{
	switch(GetRandomInt(1,3)) {
		case 1: GivePlayerItem(client, "weapon_hegrenade");
		case 2: GivePlayerItem(client, "weapon_smokegrenade");
		case 3: GivePlayerItem(client, "weapon_flashbang");
	}
}